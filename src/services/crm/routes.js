import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Secret from './components/Secret';

export default [
  {
    path: '/crm/',
    component: Home,
    exact: true
  },
  {
    path: '/crm/about',
    component: About,
    exact: true
  },
  {
    path: '/crm/contact',
    component: Contact,
    exact: true
  },
  {
    path: '/crm/secret',
    component: Secret,
    exact: true
  }
];
