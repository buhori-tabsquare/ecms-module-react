import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Header = ({ loggedIn }) => (
  <div>
    <Link to="/crm/" className="link">Home</Link>
    <Link to="/crm/about" className="link">About</Link>
    <Link to="/crm/contact" className="link">Contact</Link>
    { loggedIn && <Link to="/crm/secret" className="link">Secret</Link> }
  </div>
);

const mapStateToProps = state => ({
  loggedIn: state.loggedIn
});

export default connect(mapStateToProps)(Header);
