const crm = require('./crm/crm.service.js');

module.exports = (app) => {
  app.configure(crm);
};
