
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const path = require('path');

require( "babel-register" )( {
    presets: [ "env" ],
    plugins: [
        [
            "css-modules-transform",
            {
                camelCase: true,
                extensions: [ ".css", ".scss" ],
            },
        ],
        "dynamic-import-node",
    ],
} );
const app = express(feathers());
app.configure(configuration());
app.use( express.static(path.resolve(app.get('public'))));

const services = require('./src/services');
app.configure(services);

const port = app.get('port');
app.listen(port);