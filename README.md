# README #

This is module starter kit for agencies who using React

### How do I get set up? ###

Install dependencies with
```npm install```

Run dev mode with
```npm run dev```

Open browser
```http://localhost:3031```

### Structure? ###
env config
```./config/default.json```

webpack config
```./webpack.[module_name].config.js```

module workspace
```./src/services/[module_name]/*```